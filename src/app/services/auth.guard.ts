import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { LocalStorageProvider } from '../department-admin/servicesDp/localStorageDp';
import { routeVar } from '../constants/route-constant';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    public localstorage: LocalStorageProvider
  ) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.checkLogin();
  }
  checkLogin() {
    const IslogedIn = this.localstorage.getItem('accesstoken');
    if (IslogedIn) {
      return true;
    } else {
      this.router.navigateByUrl(`${routeVar.loginDp}`);
      return false;
    }
  }
}
