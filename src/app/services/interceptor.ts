import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, map } from 'rxjs/operators';
import {
  HttpInterceptor,
  HttpRequest,
  HttpEvent,
  HttpHandler,
  HttpResponse,
} from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { LocalStorageProvider } from '../department-admin/servicesDp/localStorageDp';
import { throwError } from 'rxjs';
import { routeVar } from '../constants/route-constant';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  counter = 0;
  constructor(
    private localStorageProvider: LocalStorageProvider,

    private route: Router
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    // const Btoken = this.localStorageProvider.getItem('accesstoken')
    //   ? this.localStorageProvider.getItem('accesstoken')
    //   : '';
    // const token = this.localStorageProvider.getItem('accesstoken')
    //   ? this.localStorageProvider.getItem('accesstoken')
    //   : '';

    // if (req.url.includes(environment.API_BASE_URL)) {
    //   req = req.clone({
    //     headers: req.headers.set('x-api-key', environment.encyptionKey).delete('Authorization', 'Bearer ' + Btoken)
    //   });

    //   if(req.url.includes(environment.API_BASE_URL+'auth/admin-login')){
    //     req = req.clone({
    //       headers: req.headers.set('x-api-key', environment.encyptionKey),
    //     });
    //   }

    //   if (Btoken) {
    //     req = req.clone({
    //       headers: req.headers.set('Authorization', 'Bearer ' + Btoken),
    //     });
    //   } else {
    //     req = req.clone({ headers: req.headers.set('accesstoken', token) });
    //   }
    // }
    const Btoken = this.localStorageProvider.getItem('accesstoken');
    const token = this.localStorageProvider.getItem('token');

    if (req.url.includes(environment.API_BASE_URL)) {
      req = req.clone({
        headers: req.headers.set('x-api-key', environment.encyptionKey),
      });

      if (req.url.includes(environment.API_BASE_URL + 'auth/admin-login')) {
        req = req.clone({
          headers: req.headers.set('x-api-key', environment.encyptionKey),
        });
      }

      if (Btoken) {
        req = req.clone({
          headers: req.headers.set('Authorization', 'Bearer ' + Btoken),
        });
      } else if (token) {
        req = req.clone({
          headers: req.headers.set('token', token),
        });
      }
    }

    return next.handle(req).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          if (event.status == 401) {
            // this.toaster.error('Session Expired', 'Error');
            // this.localStorageProvider.clearAll();
            // this.route.navigateByUrl(`${routeVar.loginDp}`);
          }
        }
        return event;
      }),
      catchError((err: any) => {
        if (err.status == 401) {
          // this.localStorageProvider.clearAll();
          // this.route.navigateByUrl(`${routeVar.loginDp}`);
        } else {
          //   this.toaster.error(err.error.message, 'Error');
        }
        return throwError(err);
      })
    );
  }
}
