import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(public http$:HttpClient) { }
	public getPostIds() {
		return this.http$.get(`${environment.API_BASE_URL}/topstories.json?print=pretty`)
			.pipe(catchError((error: HttpErrorResponse) => of(error)));
	}

	public getPostIdData(id: any) {
		return this.http$.get(`${environment.API_BASE_URL}/item/${id}.json?print=pretty`)
			.pipe(catchError((error: HttpErrorResponse) => of(error)));
	}
}
