import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WrapperRoutingModule } from './wrapper-routing.module';
import { WrapperComponent } from './wrapper/wrapper.component';
import { HeaderComponent } from './wrapper/header/header.component';
import { FooterComponent } from './wrapper/footer/footer.component';
import { BodyComponent } from './wrapper/body/body.component';
import { ItemComponent } from './wrapper/item/item.component';



@NgModule({
  declarations: [
    WrapperComponent,
    HeaderComponent,
    FooterComponent,
    BodyComponent,
    ItemComponent
  ],
  imports: [
    CommonModule,
    WrapperRoutingModule
  ]
})
export class WrapperModule { }
