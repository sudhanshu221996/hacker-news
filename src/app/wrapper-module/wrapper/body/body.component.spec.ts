import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { BodyComponent } from './body.component';
import { ApiService } from 'src/app/services/api-service';

describe('BodyComponent', () => {
  let component: BodyComponent;
  let fixture: ComponentFixture<BodyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BodyComponent],
      imports: [HttpClientTestingModule], 
      providers: [ApiService]
    });
    fixture = TestBed.createComponent(BodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('component should create', () => {
    expect(component).toBeTruthy();
  });

  it('api service should be created', () => {
    const service: ApiService = TestBed.get(ApiService);
    expect(service).toBeTruthy();
  });

  it('should have getPostIds function', () => {
    const service: ApiService = TestBed.get(ApiService);
    expect(service.getPostIds).toBeTruthy();
  });

  it('limit should start from 20 by default', () => {
    expect(component.limit).toEqual(20);
  });

  it('should fetch the post ids.', (() => {
    spyOn(component, "ngOnInit").and.callThrough();
    component.ngOnInit();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.postIds).toBeTruthy();
    });
  }));
});


