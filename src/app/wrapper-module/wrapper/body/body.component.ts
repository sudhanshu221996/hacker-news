import { Component, ElementRef, HostListener } from '@angular/core';
import { ApiService } from 'src/app/services/api-service';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.scss']
})
export class BodyComponent {
  postIds: number[] = [];
  limit: number = 20;

  constructor(private apiService: ApiService,
    private elementRef:ElementRef) {}

  ngOnInit(): void {
    this.getAllPostIds();
  }

  getAllPostIds() {
    this.apiService.getPostIds().subscribe((response) => {
      this.postIds = response as number[];
    })
  }
  
 
  @HostListener('scroll', ['$event'])
  onScroll(): void {
    const scrollableDiv = this.elementRef.nativeElement.querySelector('.hackerNew');
    const atBottom = scrollableDiv.scrollHeight - scrollableDiv.scrollTop === scrollableDiv.clientHeight;
    if (atBottom) {
      this.limit = this.limit + 20;
    }
  }
}
