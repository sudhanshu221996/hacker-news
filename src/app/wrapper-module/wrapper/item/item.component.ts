import { Component, Input } from '@angular/core';
import { ApiService } from 'src/app/services/api-service';
import * as moment from 'moment';

interface InputData {
  by: string;
  descendants: number;
  id: number;
  kids: number[];
  score: number;
  time: number;
  title: string;
  type: string;
  url?: string;
}

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent {
  @Input() storyId: number = 0;
  @Input() index: number = 0;
  
  newsData: InputData | null = null;

  constructor(
    private storyItemService: ApiService
  ) {}

  ngOnInit() {
    this.getStory();
  }

  getStory() {
    this.storyItemService.getPostIdData(this.storyId).subscribe((data) => {
      this.newsData = data as InputData;
    });
  }

  openNews(url: string) {
    if (url) {
      window.open(url, '_blank');
    }
  }

  getTime(time: number | undefined) {
    const timeStamp = time as number
    return moment(timeStamp*1000).fromNow()
  }

  getUrl(url: string): any {
    if(url) {
      return `(${(new URL(url)).hostname})`;
    } else {
      return ''
    }
  }
}
